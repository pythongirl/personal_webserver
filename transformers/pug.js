/* eslint-env node */

const path = require("path")
const pug = require("pug")
const makeTransformer = require("../makeTransformer")

module.exports = {
  ".pug": makeTransformer(function (input, sendStream, config) {
    let compiledFile = pug.compile(input, {
      basedir: path.join(config.rootDir, config.includeRoot),
      filename: "Pug"
    })

    return compiledFile({
      sendStream: sendStream,
      path: sendStream.path.replace(/^\/|\/$/g, "").split("/"),
      push(url, type){
		// Disableing push
        /*
		let previousLinks = sendStream.res.getHeader("Link") || []
        if(url[0] != "/"){
          url = sendStream.parsedUrl.pathname.split("/").slice(0, -1).join("/") + "/" + url
        }
        sendStream.res.setHeader("Link", [...previousLinks, `<${url}>; rel=preload; as=${type}`])
		*/
      }
    })
  }, "text/html")
}

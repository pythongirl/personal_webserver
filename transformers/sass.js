/* eslint-env node */

const fs = require("fs")
const path = require("path")
const sass = require("node-sass")

const makeTransformer = require("../makeTransformer.js")

let sass_transformer = makeTransformer(async function(input, sendStream, config){

  function cacheFile(files, resolvedPath){
    files[path.join("..", path.resolve(resolvedPath))] = {
      remoteUrl: path.join("/", path.relative(config.rootDir, resolvedPath)),
      contents: fs.readFileSync(mainFile, "utf-8")
    }
  }

  let mainFile = path.resolve(path.join(config.rootDir, sendStream.parsedUrl.pathname))
  let files = {}
  cacheFile(files, mainFile)

  let result = sass.renderSync({
    file: mainFile,
    includePaths: [config.rootDir],
    outputStyle: "compressed",
    outFile: sendStream.parsedUrl.pathname,
    sourceMap: sendStream.parsedUrl.pathname + "?map",
    importer: function(url, prev){
      let resolvedPath
      if(url[0] == "/"){
        resolvedPath = path.join(config.rootDir, url)
      } else {
        resolvedPath = path.resolve(path.parse(prev).dir, url)
      }

      cacheFile(files, resolvedPath)

      return {file: resolvedPath}
    }
  })

  if (sendStream.hasQueryParam("map")) {
    let sourceMap = JSON.parse(result.map)
    sourceMap.sourcesContent = []

    for(let [index, source] of sourceMap.sources.entries()){
      sourceMap.sources[index] = files[source].remoteUrl
      sourceMap.sourcesContent[index] = files[source].contents
    }

    return JSON.stringify(sourceMap)
  } else {
    return result.css
  }
}, "text/css")

module.exports = {
  ".sass": sass_transformer,
  ".scss": sass_transformer
}

/* eslint-env node */

const babel = require("@babel/core")

const makeTransformer = require("../makeTransformer.js")

function makeBabelTransformer(options) {
  return makeTransformer(async function (input, sendStream) {
    let result = await babel.transformAsync(input, { "sourceMaps": true, ...options })

    if (sendStream.hasQueryParam("map")) {
      result.map.sources = [sendStream.parsedUrl.pathname]
      result.map.sourcesContent = [input.toString()]
      return JSON.stringify(result.map)
    } else {
      return result.code + "\n//# sourceMappingURL=" + sendStream.parsedUrl.pathname + "?map"
    }
  }, "text/javascript")
}

module.exports = {
  // ".js": makeBabelTransformer({presets: []}),
  ".ts": makeBabelTransformer({ plugins: ["@babel/plugin-transform-typescript"], presets: [] }),
  ".jsx": makeBabelTransformer({ plugins: ["@babel/plugin-transform-react-jsx"], presets: [] }),
  ".tsx": makeBabelTransformer({
    plugins: [
      ["@babel/plugin-transform-typescript", { isTSX: true }],
      "@babel/plugin-transform-react-jsx"
    ], presets: []
  })
}
/* eslint-env node */

const less = require("less")
const makeTransformer = require("../makeTransformer")

module.exports = {
  ".less": makeTransformer(async function (input, sendStream){
    let result = await less.render(input.toString(), {
      sourceMap: {
        sourceMapURL: sendStream.parsedUrl.pathname + "?map"
      },
      filename: sendStream.parsedUrl.pathname + "?original"
    })

    if (sendStream.hasQueryParam("map")) {
      return result.map
    } else {
      return result.css
    }
  }, "text/css")
}
#!/usr/bin/env node

/* eslint-env node */

const fs = require("fs")
const path = require("path")
const http = require("http")
const send = require("send")
const url = require("url")

require("dotenv").config()

let config = {
	"port": parseInt(process.env.WEBSERVER_PORT) || 80,
	"host": process.env.WEBSERVER_HOST || "0.0.0.0",
	"documentRoot": process.env.WEBSERVER_DOCUMENT_ROOT,
	"includeRoot": process.env.WEBSERVER_INCLUDE_ROOT,
	"logFile": process.env.WEBSERVER_LOG || "/var/log/webserver.log",
}

if(config.documentRoot == undefined){
	console.error("The document root must be set with the WEBSERVER_DOCUMENT_ROOT environment variable.")
	process.exit(1)
}

if(config.includeRoot == undefined){
	console.error("The include root must be set with the WEBSERVER_INCLUDE_ROOT environment variable.")
	process.exit(1)
}


config.rootDir = config.documentRoot

let transformations = {
  ...require("./transformers/pug"),
  ...require("./transformers/babel"),
  ...require("./transformers/sass"),
  ...require("./transformers/coffeescript")
}

var server = http.createServer(function onRequest(req, res) {

  fs.appendFile(config.logFile, `${new Date().toISOString()} [${req.headers["x-forwarded-for"]}] ${req.method} ${req.url} (${req.headers["user-agent"]})\n`, (err) => {
    if(err){
      console.error("Unable to append to file:\n", err)
    }
  })

  send(req, url.parse(req.url).pathname, {
    root: config.rootDir,
    extensions: ["html", "pug"],
    index: ["index.html", "index.pug"],
    maxAge: "4h",
	dotfiles: "ignore"
  })
    .on("stream", function (fileStream) {
      let sendStream = this

      this.parsedUrl = url.parse(this.req.url, true)
      sendStream.hasQueryParam = param => Object.prototype.hasOwnProperty.call(sendStream.parsedUrl.query, param)

      if (sendStream.hasQueryParam("original")) {
        sendStream.res.setHeader("content-type", "text/plain")
        return
      }

      let extension = path.extname(fileStream.path)

      if (transformations.hasOwnProperty(extension)) {
        fileStream.pipe = function(){}
        transformations[extension](fileStream, sendStream, config).then(s => s.pipe(this.res)).catch( e => console.error(e) )
      }
    })
    .pipe(res)
})

server.listen(config.port, config.host, () => console.log(`Running on port ${config.host}:${config.port}`))

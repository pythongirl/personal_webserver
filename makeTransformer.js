/* eslint-env node */

const fs = require("fs")
const stream = require("stream")
const crypto = require("crypto")

module.exports = function makeTransformer(transformer, outputType) {
  return async function (fileStream, sendStream, config) {
    let fileContents = fs.readFileSync(fileStream.path)

    let processedFile
    try {
      processedFile = await transformer(fileContents, sendStream, config)
    } catch(e) {
      let errorHash = crypto.createHash("SHA1")
      errorHash.update(e.stack)
      let errorDigest = errorHash.digest("hex")

      sendStream.res.statusCode = 500
      sendStream.res.setHeader("content-type", "text/plain")
      sendStream.res.end(`Template Error\nHash: ${errorDigest}\n`)

      console.error(`Template Error with hash <${errorDigest}>`, e)
    }


    let buffer = Buffer.from(processedFile, "utf-8")
    sendStream.res.setHeader("content-type", outputType)
    sendStream.res.setHeader("content-length", buffer.length)

    let replacementStream = new stream.Readable()
    replacementStream.push(buffer)
    replacementStream.push(null)

    return replacementStream
  }
}
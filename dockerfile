FROM node:11-alpine

WORKDIR /usr/src/webserver

COPY . .

RUN npm install

EXPOSE 80
CMD npm start

